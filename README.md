# QupyRibbon
![alt tag](http://i.imgur.com/ry2SudV.png)

This is a ribbon implementation in Python 3 using PyQt5.

To use this you need to get the dependencies:
```
sudo apt install python3
sudo apt install python3-pyqt5
```

The project is made in Pycharm community eddition.
For example clone the project:
```
git clone https://gitlab.com/magnusmj/QupyRibbon.git
```
and run:
```
python3 main.py 
```
